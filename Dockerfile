FROM debian

RUN apt-get install -y cowsay

COMMAND cowsay $(date)